import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ApolloProvider  } from 'react-apollo'
import store, { client } from './store'
import { App } from './components'
import './index.css'

ReactDOM.render(
  <Provider store={store}>
    <ApolloProvider store={store} client={client}>
      <App />
    </ApolloProvider>
  </Provider>,
  document.getElementById('root')
)

