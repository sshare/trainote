import React from 'react'
import _ from 'lodash'
import { Tabs, Tab } from '@material-ui/core'
import { compose, withState } from 'recompose'

export default compose(
  withState('active', 'setActive', 0)
)(({
  active,
  data = [],
  className = ''
}) => (
  <div className={`tabs ${className}`}>
    <Tabs>
      {_.map(data, ({ tab }) => (
        <Tab
          key={`tab-${tab.label}`}
          {...tab}
        />
      ))}
    </Tabs>

    {_.get(data, `[${active}].content`)}
  </div>
))

