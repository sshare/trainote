import React, { Component } from 'react'
import { CssBaseline } from '@material-ui/core'

export class Page extends Component {
  render () {
    const {
      children,
      header = (<header></header>),
      className
    } = this.props

    return (
      <div className={`Page ${className}`}>
        <CssBaseline />
        {header}
        {children}
      </div>
    )
  }
}

export default Page

