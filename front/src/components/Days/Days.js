import _ from 'lodash'
import { compose, withState, withProps, withHandlers } from 'recompose'
import View from './View'

export default compose(
  withState('activeDay', 'setActiveDay', 0),
  withProps(({ data, date }) => ({
    user: JSON.parse(_.get(data, 'user.attrs', '{}'))
  })),
  withHandlers({
    selectDay: props => (e, value) => props.setActiveDay(value)
  })
)(View)
