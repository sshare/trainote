import React from 'react'
import _ from 'lodash'
import Page from '../common/Page/Page'
import { Tabs, Tab, AppBar, Avatar, Toolbar } from '@material-ui/core'
import { Day } from '..'
import './Days.css'

const toolbarStyles = {
  minHeight: 'auto',
  justifyContent: 'space-between'
}

export default ({ activeDay, selectDay, dates, user }) => (
  <Page className='Days-select-day'>
    <AppBar position='static'>
      <Toolbar disableGutters style={toolbarStyles}>
        <Tabs value={activeDay} onChange={selectDay}>
          {_.map([0, 1, 2], i => (
            <Tab
              key={`tab-days-${i}`}
              label={`Day ${i}`}
              value={i}
            />
          ))}
        </Tabs>

        <Avatar
          style={{ float: 'right' }}
          alt='ava'
          src={_.get(user, 'picture.data.url')}
        />
      </Toolbar>
    </AppBar>

    <Day activeDay={activeDay} />
  </Page>
)
