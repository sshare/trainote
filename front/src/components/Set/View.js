import React from 'react'
import _ from 'lodash'
import { TextField } from '@material-ui/core'
import './Set.css'

const textFieldSyle = {
  height: '30%'
}

const inputProps = {
  fullWidth: true
}

export default ({
  data: {
    set: {
      exercise = {},
      weight = '',
      repetitions = ''
    } = {}
  },
  date,
  edit,
  onInput
}) => (
  <div className='Set'>
    <TextField
      placeholder='weight'
      value={_.get(edit, 'weight', weight) || ''}
      onChange={onInput('weight')}
      style={textFieldSyle}
      {...inputProps}
    />

    <TextField
      placeholder='count'
      value={_.get(edit, 'repetitions', repetitions) || ''}
      onChange={onInput('repetitions')}
      style={textFieldSyle}
      {...inputProps}
    />
  </div>
)

