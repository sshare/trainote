import { connect } from 'react-redux'
import _ from 'lodash'
import { compose, pure, withState, withHandlers } from 'recompose'
import gql from 'graphql-tag'
import { graphql, withApollo } from 'react-apollo'
import View from './View'

export const QUERY = gql`
  query Query($id: Int!) {
    set(id: $id) {
      id,
      weight,
      repetitions,
      user,
      exercise {
        id,
        title
      },
      programm {
        id
      }
    }
  }
`
const options = ({ id = 0 }) => ({
  variables: { id }
})

const MUTATION = gql`
  mutation updateSet($attributes: String) {
    updateSet(attributes: $attributes) {
      id,
      weight,
      repetitions,
      exercise {
        id,
        title
      }
    }
  }
`
const props = ({ mutate, ownProps: { id } }) => ({
  onChange: variables =>
    mutate({
      variables,
      refetchQueries: [{ query: QUERY, variables: { id } }]
    })
})

const onInput = ({
  onChange,
  edit,
  setEdit,
  data: {
    set,
    user: { id } = {}
  }
}) => field => e => {
  const newSet = { ...set, ...edit }
  _.set(newSet, field, e.target.value)

  if (field === 'exercise.title') {
    _.set(newSet, 'exercise', {
      id: newSet.exercise.id,
      title: e.target.value
    })
  }

  setEdit(newSet)
  onChange({
    attributes: JSON.stringify({
      ...newSet,
      user: { id }
    })
  })
}

export default compose(
  connect(state => ({
    user: state.auth.user
  })),
  graphql(QUERY, { options }),
  graphql(MUTATION, { props }),
  withApollo,
  withState('edit', 'setEdit', {}),
  withHandlers({ onInput }),
  pure
)(View)
