import React from 'react'
import FacebookAuth from 'react-facebook-auth'
import config from '../../config'

export default ({ signIn }) => (
  <FacebookAuth
    {...config.facebook} 
    callback={signIn}
    component={({ onClick }) => (
      <button
        onClick={onClick}
        style={{ fontSize: 16, border: '1px solid', color: 'blue' }}
      >
        Login with Facebook
      </button>
    )}
  />
)

