import { compose, pure } from 'recompose'
import View from './View'

export default compose(pure)(View)

