import React from 'react'
import Page from '../common/Page/Page'
import LoginButton from './LoginButton'

export default ({ user, onSignIn }) => (
  <Page
    className='login'
    header={!!user && (
      <header className='App-header'>
        <img src={null} className='App-logo' alt='logo' />
        <h1 className='App-title'>Welcome, please sign in</h1>
      </header>
    )}
  >
    <div>
      <br />
      <LoginButton signIn={onSignIn}/>
    </div>
  </Page>
)

