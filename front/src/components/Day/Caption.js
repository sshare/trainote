import React from 'react'
import { ListItem, Typography } from '@material-ui/core'

export default ({ children }) => (
  <ListItem
    divider
    dense
    style={{ height: 30 }}
  >
    <Typography
      variant='caption'
      gutterBottom
      align='center'
    >
      {children}
    </Typography>
  </ListItem>
)
