import { connect } from 'react-redux'
import _ from 'lodash'
import moment, { timestamp } from '../../helpers/date'
import { compose, withProps } from 'recompose'
import gql from 'graphql-tag'
import { graphql, withApollo } from 'react-apollo'
import View from './View'

export const USER_QUERY = gql`
  query Query($token: String) {
    user(token: $token) {
      id,
      attrs
    }
  }
`
const userOptions = ({ token }) => ({
  variables: { token }
})
export const QUERY = gql`
  query Query($date: Float!, $userId: Int!) {
    sets(date: $date, userId: $userId) {
      id
    }
  }
`
const options = ({
  date = timestamp(),
  data: { user: { id } = {} } = {}
}) => ({
  variables: { date, userId: id }
})

const MUTATION = gql`
  mutation update($attributes: String) {
    updateSet(attributes: $attributes) {
      id
    }
  }
`
const props = ({
  mutate,
  ownProps: {
    onAddSet,
    date,
    data: { variables: { userId } = {} }
  }
}) => ({
  onAddSet: () => {
    console.log({ userId })
    mutate({
      variables: {
        attributes: JSON.stringify({ user: { id: userId } })
      },
      refetchQueries: [{
        query: QUERY,
        variables: { date, userId }
      }]
    })
  }
})

const days = _.times(31, day => day - 15)

export default compose(
  connect(state => ({
    token: state.auth.token
  })),
  graphql(USER_QUERY, { options: userOptions }),
  graphql(QUERY, { options }),
  graphql(MUTATION, { props }),
  withApollo,
  withProps(({ data, date }) => ({
    dates: _.map(days, delta =>
      timestamp(moment().add(delta, 'days'))
    ),
    exercises: _.times(5),
    sets: _.times(5),
    ..._.pick(data, ['sets', 'exercises'])
  }))
)(View)
