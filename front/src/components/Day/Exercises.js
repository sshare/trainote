import React from 'react'
import _ from 'lodash'
import { List, ListItem } from '@material-ui/core'
import Caption from './Caption'

export default ({ exercises }) => (
  <List className='exercises'>
    <Caption>Exercises</Caption>

    {_.map(exercises, i => (
      <ListItem
        key={`exercises-${i}`}
        divider
        dense
        style={{ height: 65 }}
      >
        {i + 1}
      </ListItem>
    ))}
  </List>
)
