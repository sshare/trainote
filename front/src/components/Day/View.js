import React from 'react'
import { Paper } from '@material-ui/core'
import './Day.css'
import Exercises from './Exercises'
import Sets from './Sets'

export default ({ sets, exercises, dates, onAddSet }) => (
  <Paper className='Day'>
    <Exercises exercises={exercises} />

    <Sets
      dates={dates}
      sets={sets}
    />

    <button onCllck={onAddSet}>Add</button>
  </Paper>
)
