import React from 'react'
import _ from 'lodash'
import { List, ListItem } from '@material-ui/core'
import Slider from 'react-slick'
import { dateFormat } from '../../helpers/date'
import { Set } from '..'
import Caption from './Caption'

const sliderSettings = {
  infinite: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  swipeToSlide: true,
  accessibility: true,
  adaptiveHeight: true,
  variableWidth: false,
  slide: 'List',
  arrows: false
}

export default ({ dates, sets }) => (
  <div className='sets'>
    <Slider
      {...sliderSettings}
      initialSlide={_.floor(_.size(dates) / 2)}
    >
      {_.map(dates, date => (
        <List key={`sets-list-date-${date}`}>
          <Caption>{dateFormat(date)}</Caption>

          {_.map(sets, (set, i) => (
            <ListItem
              key={`set-${date}-${i}-${set.id}`}
              divider
              dense
              style={{ height: 65 }}
            >
              <Set
                key={`set-block-${set.id}-${i}`}
                id={set.id}
              />
            </ListItem>
          ))}
        </List>
      ))}
    </Slider>
  </div>
)
