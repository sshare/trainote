export { default as App } from './App/App'
export { default as Login } from './Login/Login'
export { default as Days } from './Days/Days'
export { default as Day } from './Day/Day'
export { default as Set } from './Set/Set'
export { default as Privacy } from './PrivacyPolicy/PrivacyPolicy'

