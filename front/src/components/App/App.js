import { connect } from 'react-redux'
import { compose, withHandlers } from 'recompose'
import _ from 'lodash'
import gql from 'graphql-tag'
import { withStyles } from '@material-ui/core/styles'
import { graphql, withApollo } from 'react-apollo'
import { signIn } from '../../store/reducers/auth'
import View from './View'

const userID = '123456789'
const defaultUser = {
  'name': 'Test user',
  'email': 'test@test.com',
  'picture': { 'data': { 'url': 'https://cdn1.iconfinder.com/data/icons/sports-32/64/sport-30-64.png' } },
  'id': userID,
  'accessToken': 'testtoken',
  'userID': userID
}

export const QUERY = gql`
  query Query($token: String) {
    user(token: $token) {
      id,
      attrs
    }
  }
`
const options = ({ token }) => ({
  variables: { token }
})
const MUTATION = gql`
  mutation login($user: String!) {
    login(user: $user) {
      id,
      attrs
    }
  }
`
const props = ({ mutate, ownProps: { token } }) => ({
  login: variables =>
    mutate({
      variables,
      refetchQueries: [{ query: QUERY, variables: { token } }]
    })
})

const onSignIn = ({ signIn, login }) => async (response) => {
  const loggedUser = response.status ? response : defaultUser
  const token = _.get(loggedUser, 'accessToken')

  signIn({ token })
  login({ user: JSON.stringify(loggedUser) })
}

export default compose(
  connect(state => ({
    token: state.auth.token
  }), { signIn }),
  graphql(QUERY, { options }),
  graphql(MUTATION, { props }),
  withApollo,
  withHandlers({ onSignIn }),
  withStyles(() => ({}))
)(View)
