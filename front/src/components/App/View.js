import React, { Component } from 'react'
import _ from 'lodash'
import './App.css'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import { Login, Days, Privacy } from '..'

export default class View extends Component {
  constructor (props) {
    super(props)
    this.renderDays = this.renderDays.bind(this)
  }

  renderDays () {
    const {
      data: { user }
    } = this.props
    const Page = _.get(user, 'id') ? Days : Login

    return (
      <Page {...this.props} />
    )
  }

  render () {
    const { classes } = this.props

    return (
      <div>
        <Router>
          <div className={`App ${classes.root || ''}`}>
            <Route path='/privacy' component={Privacy} />
            <Route path='/' render={this.renderDays} />
          </div>
        </Router>
      </div>
    )
  }
}
