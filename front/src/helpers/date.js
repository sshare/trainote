import moment from 'moment'

export default  moment

export const dateFormat = date => moment.unix(date).format('YY-MM-DD')

export const timestamp = date =>  moment(date).unix()

