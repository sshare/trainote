const authCookieName = 'auth_token'
const maxAge = 3600000 * 24

const apiUtils = {
  saveAuthCookie (value) {
    const date = new Date(Date.now() + maxAge)
    document.cookie = `${authCookieName}=${value}; path=/; expires=${date.toUTCString()}`
  },

  deleteAuthCookie (req, key) {
    const date = new Date(Date.now() - 1)
    document.cookie = `${key || authCookieName}=; path=/; expires=${date.toUTCString()}`
  },

  readAuthCookie (req) {
    const cookie = document.cookie

    if (cookie) {
      const matches = cookie.match(new RegExp(`(?:^|; )${authCookieName}=([^;]*)`))
      return matches ? decodeURIComponent(matches[1]) : undefined
    }

    return ''
  },

  clearAllCookies (req) {
    const cookies = document.cookie.split(';')

    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i]
      const eqPos = cookie.indexOf('=')
      const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie
      apiUtils.deleteAuthCookie(null, name)
    }
  }
}

export default apiUtils
