import cookie from '../../helpers/cookie'

const initialState = {
  token: cookie.readAuthCookie()
}
const SIGN_IN = 'trainote/auth/SIGN_IN'

export default (state = initialState, action) => {
  switch (action.type) {
    case SIGN_IN: {
      return {
        ...state,
        token: action.token
      }
    }
    default:
      return state
  }
}

export function signIn ({ token }) {
  if (token) {
    cookie.saveAuthCookie(token)
  }

  return {
    type: SIGN_IN,
    token
  }
}
