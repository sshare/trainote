const initialState = {
  user: null
}
const SAVE = 'trainote/table/SAVE'

export default (state = initialState, action) => {
  switch (action.type) {
    case SAVE:
      return {
        ...state,
        ...action
      }

    default:
      return state
  }
}

export function save (data) {
  return {
    type: SAVE,
    ...data
  }
}

