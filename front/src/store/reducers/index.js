import auth from './auth'
import table from './table'
import client from '../client'

export default {
  auth,
  table,
  apollo: client.reducer()
}

