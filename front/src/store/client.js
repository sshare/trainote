import ApolloClient, { createNetworkInterface, addTypenameToDocument  } from 'apollo-client'
import { InMemoryCache  } from 'apollo-cache-inmemory'
import config from '../config'

export default new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: config.api.url
  }),
  queryTransformer: addTypenameToDocument,
  dataIdFromObject: ({ id, __typename }) => (id && __typename) ? __typename + id : null, // eslint-disable-line no-underscore-dangle
  initialState: global.__APOLLO_STATE__, // eslint-disable-line no-underscore-dangle
  cache: new InMemoryCache()
})

