import { createStore, combineReducers, applyMiddleware } from 'redux'
import _ from 'lodash'
import client from './client'
import reducers from './reducers'

const devTool = _.get(global, '__REDUX_DEVTOOLS_EXTENSION__', v => v)()

export default createStore(
  combineReducers(reducers),
  applyMiddleware(client.middleware()),
  devTool
)

export { client }

