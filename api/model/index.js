const _ = require('lodash')
const Sequelize = require('sequelize')
const {
  db: {
    username,
    password,
    database,
    host,
    dialect
  }
} = require('../config')
const defineEntities = require('./entities')

const connector = new Sequelize(database, username, password, {
  host,
  dialect,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
})
const definedEntities =  defineEntities(connector)

module.exports = {
  sync: async app => {
    await connector.query('SET FOREIGN_KEY_CHECKS = 0;', { raw: true })
    await connector.query('SET NAMES utf8;')
    return connector.sync({ alter: true })
  },
  ...definedEntities
}

