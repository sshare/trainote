const Sequelize = require('sequelize')

const defaultFields = {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  createdAt: Sequelize.DATE
}

module.exports = connector => {
  const User = connector.define('user', {
    ...defaultFields,
    token: Sequelize.STRING,
    attrs: Sequelize.JSON
  })

  const Exercise = connector.define('exercise', {
    ...defaultFields,
    title: Sequelize.STRING,
    description: Sequelize.STRING
  })

  const Programm = connector.define('programm', {
    ...defaultFields,
    title: {
      type: Sequelize.STRING,
      primaryKey: true,
      defaultValue: ''
    }
  })

  const Set = connector.define('set', {
    ...defaultFields,
    weight: Sequelize.STRING,
    repetitions: Sequelize.STRING
  })

  User.belongsTo(User, { as: 'trainer' })
  User.hasMany(Set)

  Programm.belongsTo(User, { as: 'trainer' })
  Programm.hasMany(Exercise, { as: 'exercises' })

  Set.belongsTo(Programm, { constraints: false })
  Set.belongsTo(Exercise, { constraints: false })
  Set.belongsTo(User, { constraints: false })

  return {
    User,
    Exercise,
    Programm,
    Set
  }
}

