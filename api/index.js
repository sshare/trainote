const express = require('express')
const cors = require('cors')
const applyGraphql = require('./graphql')
const { sync: syncModel } = require('./model')
const { server: { host, port } } = require('./config')

async function start (app) {
  app.use(cors())
  applyGraphql(app)
  await syncModel(app)
  app.listen(port)
  console.log(`Server listening on http://${host}:${port} ...`)
}

start(express())
