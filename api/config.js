const _ = require('lodash')
const appName = 'TRAINOTE'

function getFromEnv (varName, defaultValue = '', prefix = 'ROOT') {
  return _.get(process.env, `${appName}_${_.upperCase(prefix)}_${_.upperCase(varName)}`, defaultValue)
}

const db = {
  username: getFromEnv('username', 'root', 'db'),
  password: getFromEnv('password', null, 'db'),
  database: getFromEnv('database', 'trainote', 'db'),
  host: getFromEnv('host', 'db', 'db'),
  dialect: 'mysql',
  dialectOptions: {
    charset: 'UTF-8'
  }
}

const server = {
  host: getFromEnv('host', 'trainote', 'server'),
  port: getFromEnv('port', 3002, 'server')
}

const app = {
  trainerId: 1175115525,
  endpointURL: '/api'
}

module.exports = {
  server,
  db,
  app
}
