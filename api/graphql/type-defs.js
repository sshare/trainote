require('graphql-tools')

module.exports = `
  type User {
    id: Int!,
    token: String!,
    attrs: String,
    createdAt: Float!
  }

  type Programm {
    id: Int!,
    title: String
  }

  type Exercise {
    id: Int!,
    title: String
  }

  type Set {
    id: Int,
    weight: String,
    repetitions: String,
    user: Int,
    exercise: Exercise,
    programm: Programm
  }

  type Query {
    sets(date: Float!, userId: Int!): [Set]
    set(id: Int!): Set,
    user(token: String): User
  }

  type Mutation {
    login(user: String!): User
    updateSet(attributes: String): Set
  }
`
