const _ = require('lodash')
const moment = require('moment')
const { Op } = require('sequelize')
const { User, Exercise, Programm, Set } = require('../model')

module.exports = {
  Query: {
    sets: async (root, { date: timestamp, userId }) => {
      const date = moment(timestamp * 1000).utc()

      const found = await Set.findAll({
        where: {
          userId,
          createdAt: {
            [Op.gt]: date.startOf('day').format(),
            [Op.lt]: date.endOf('day')
          }
        }
      })

      return _.map(_.times(5), i => ({
        id: 0,
        ..._.get(found, i)
      }))
    },
    set: async (root, { id }) => {
      const found = await Set.findOne({
        where: { id },
        include: [ Exercise ]
      })

      return {
        ..._.get(found, 'dataValues'),
        exercise: _.get(found, 'dataValues.exercise.dataValues')
      }
    },
    user: async (root, { token }) => {
      const user = await User.findOne({
        where: { token }
      })

      return user ? {
        id: user.id,
        attrs: JSON.stringify(user.get())
      } : null
    }
  },
  Mutation: {
    login: async (root, { user }) => {
      const userData = JSON.parse(user)
      const userId = _.get(userData, 'id')

      await User.findOrCreate({
        where: {
          id: userId
        },
        defaults: {
          id: userId,
          token: userData.accessToken,
          ...userData
        }
      })
        .spread(({ dataValues }) => ({
          ...dataValues,
          id: _.toInteger(dataValues.id)
        }))

      return {
        id: userId,
        token: userData.accessToken,
        ...userData
      }
    },
    updateSet: async (root, { attributes }) => {
      const {
        id,
        user: { id: userId },
        exercise,
        programmId = 1,
        ...fields
      } = JSON.parse(attributes || '{}')

      const user = await User.findOne({ where: { id: userId } })

      const programm = await Programm
        .findOrCreate({ where: { id: programmId } })
        .spread(({ dataValues: { id } }) => ({ id }))

      const newExercise = await Exercise
        .findOrCreate({
          where: { id: _.get(exercise, 'id') }
        })
        .spread(async ({ dataValues: { id }, created }) => {
          if (!created) {
            await Exercise.update({
              title: _.get(exercise, 'title'),
              programmId
            }, {
              where: { id }
            })
          }

          return { id }
        })

      const created = await Set.upsert({
        id,
        ...fields,
        userId,
        programmId,
        exerciseId: newExercise.id,
        exercise: newExercise
      }, { returning: true })

      const result = await Set.findOne({
        ...(created ? {}
          : { where: { id } }
        ),
        order: [ [ 'createdAt', 'DESC' ] ]
      })

      return result.dataValues
    }
  }
}
