const { app: { endpointURL } } = require('../config')
const { graphqlExpress, graphiqlExpress  } = require('apollo-server-express')
const { makeExecutableSchema  } = require('graphql-tools')
const bodyParser = require('body-parser')
const typeDefs = require('./type-defs')
const resolvers = require('./resolvers')

const myGraphQLSchema = makeExecutableSchema({ typeDefs, resolvers  })

module.exports = app => app
  .use(
    endpointURL,
    bodyParser.json(),
    graphqlExpress({
      schema: myGraphQLSchema,
      context: {},
      //tracing: true,
      cacheControl: true
    })
  )
  .get(
    endpointURL,
    graphiqlExpress({ endpointURL })
  )

